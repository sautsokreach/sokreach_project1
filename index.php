<script type="text/javascript" src="jquery.js"></script>
<html>
  <style>
    #exam{
      font-size: 100px;
    }
    .exam{
      padding-left: 20px;
      height:40px;
      font-size: 25px;
    }
    input[type=radio] {
    margin-top:20px;
    transform: scale(1.5, 1.5);
    -moz-transform: scale(1.5, 1.5);
    -ms-transform: scale(1.5, 1.5);
    -webkit-transform: scale(1.5, 1.5);
    -o-transform: scale(1.5, 1.5);
}
    </style>
  <form>
    <input id="korean" type="text" placeholder="Please input korean"> 
    <input id="khmer" type="text" placeholder="Please input khmer"> 
    <input id="btn_add" type="button" value="Add">
    <br>
    <input type="button" value="Korean-khmer" id="exam_khmer" > 
    <input type="button" value="Khmer-Korean" id="exam_korean">
    <input type="button"  value="randomKhmer" id="randomKhmer">
    <input type="button"  value="randomKorean" id="randomKorean">
    <input type="button" value="Check" id="check">
    <label>Score:</label><label id="score"></label>
    <div id="exam" > </div>
  </form>
</html>
<script>
  var i = 1;
    $("#btn_add").click(function (e) {
                $.ajax({
                    type: 'Post',
                    url: 'ajax.php',
                    dataType: "json",
                    data: { korean: $("#korean").val(),khmer:$("#khmer").val() },
                    success: function (data) {
                      console.log(data)
                       if(data.status){
                        alert("Successfull");
                       }else{
                        alert("Not Successfull");
                       }
                       $("#korean").val(""); 
                       $("#khmer").val("");    

                    }
                });
    });
    $("#exam_khmer").click(function (e) {
      i=1;
                $.ajax({
                    type: 'get',
                    url: 'ajax.php',
                    dataType: "json",
                    data: { khmer:1 },
                    success: function (data) {
                      console.log(data);
                      $("#exam").html("");      
                      $.each(data, function( key, value ) {
                        $("#exam").append("<div>"+value.korean+" : <input name=exam"+key+" class='exam' value='"+value.add+"' type='radio'>"+value.add+"<input name=exam"+key+" value='"+value.add1+"' class='exam' type='radio'>"+value.add1+"<input name=exam"+key+" value='"+value.add2+"' class='exam' type='radio'>"+value.add2+"<input class='result' type='hidden' value='"+value.khmer+"'></div>");
                      });
                    }
                });
              });
    $("#exam_korean").click(function (e) {
      i=2;
      $.ajax({
          type: 'get',
          url: 'ajax.php',
          dataType: "json",
          data: { korean:1 },
          success: function (data) {
            $("#exam").html("");      
            $.each(data, function( key, value ) {
              $("#exam").append("<div>"+value.id+":"+value.khmer+" : <input class='exam'> <input class='result' type='hidden' value='"+value.korean+"'></div>");
            });
          }
      });
    });
              
      $(document).on('click', '#check', function(){
        if(i==1){
          var count=0,total=0;
        $('.exam').each(function(i, obj) {
          total++;
          if($(this).is(':checked')){
            if($(this).val()!=$(this).parent().find(".result").val()){
                $(this).css({"accent-color":"red"});
            }else{
              $(this).css({"accent-color":"blue"});
            count++;
            }
          }
          });
          $("#score").html(count+"/"+total/3);
        }else{
          var count=0,total=0;
        $('.exam').each(function(i, obj) {
          total++;
            if($(this).val()!=$(this).parent().find(".result").val()){
                $(this).css({"color":"red"});
            }else{
            count++;
            $(this).css({"color":"green"});
            }
          });
          $("#score").html(count+"/"+total);
        }
  
      });
      $(document).on('click', '#randomKhmer', function(){
      setInterval(() => {
        $.ajax({
            type: 'get',
            url: 'ajax.php',
            dataType: "json",
            data: { random:1 },
            success: function (data) {
              $("#exam").html("");      
              $("#exam").html(data.khmer);  
            }
        });
      }, 5000);
        
      });
      $(document).on('click', '#randomKorean', function(){
      setInterval(() => {
        $.ajax({
            type: 'get',
            url: 'ajax.php',
            dataType: "json",
            data: { random:1 },
            success: function (data) {
              $("#exam").html("");      
              $("#exam").html(data.korean);  
            }
        });
      }, 5000);
        
      });
 
</script>
